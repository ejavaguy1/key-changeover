package info.ejava.examples.k8s.example.config_server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

@SpringBootApplication
@EnableConfigServer
@Slf4j
public class ConfigServer {
    public static void main(String...args) {
        log.info("KEY_STORE={}", System.getenv("KEY_STORE"));
        log.info("KEY_PASS={}", System.getenv("KEY_PASS"));
        SpringApplication.run(ConfigServer.class, args);
    }

    @Value("${server.ssl.key-store:}")
    String keyStore;
    @Value("${server.ssl.key-store-password:}")
    String keyPass;

    @Bean
    CommandLineRunner debug() {
        return args->{
            log.info("server.ssl.key-store={}", keyStore);
            log.info("server.ssl.key-store-password={}", keyPass);
        };
    }

    @Bean
    public CommonsRequestLoggingFilter logFilter() {
        CommonsRequestLoggingFilter filter
                = new CommonsRequestLoggingFilter();
        filter.setIncludeQueryString(true);
        filter.setIncludePayload(true);
        filter.setMaxPayloadLength(10000);
        filter.setIncludeHeaders(true);
        filter.setAfterMessagePrefix("RQ DATA : ");
        return filter;
    }}
