#!/bin/bash -x

cp key2/keystore2.p12 ./keystore.p12
keytool -importkeystore \
  -srckeystore ./key1/keystore1.p12 -destkeystore ./keystore.p12 \
  -srcstorepass $(cat ./key1/keyWord)  -deststorepass $(cat key2/keyWord) \
  -srcalias 1 -destalias key1
