#!/bin/bash -x

KEYNO=1

mkdir -p key${KEYNO}
rm key${KEYNO}/*
pushd key${KEYNO}

#
# generate your private key and public certificate
#
openssl req \
-newkey rsa:2048 \
-nodes \
-x509 \
-days 1 \
-subj '/C=us/O=ejava.info/CN=key_changeover.ejava.info' \
-keyout key${KEYNO}.pem \
-out cert${KEYNO}.pem

#
# generate JVM keystore and truststore
#
# generate a password for keystore and truststore
openssl rand -base64 20 > keyWord
echo "changeme" > trustWord

#combine the cert and key into a password-protected keystore
openssl pkcs12 -inkey key${KEYNO}.pem -in cert${KEYNO}.pem -export -out keystore${KEYNO}.p12 -passout pass:$(cat keyWord)

#review the keystore
openssl pkcs12 -in keystore${KEYNO}.p12 -noout -info -passin pass:$(cat keyWord)

#generate a ssh key
openssl rsa -in key${KEYNO}.pem -out id_rsa_key${KEYNO}
chmod 600 id_rsa_key${KEYNO}
ssh-keygen -y -f id_rsa_key${KEYNO} > id_rsa_key${KEYNO}.pub

popd
